package org.buzanovn.simplex.math

import java.io.Serializable

class ObjectiveFunction(val coefficients: FractionVector,
                        val absoluteTerm: Double,
                        val problem: ProblemType) : Serializable {

    enum class ProblemType(val value: String) {
        MIN("min"),
        MAX("max")
    }
}