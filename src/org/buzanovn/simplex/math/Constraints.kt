package org.buzanovn.simplex.math

import java.io.Serializable

/**
 * A wrapped type that contains all the constraints
 * of the optimization problem given
 */
class Constraints : Serializable {
    val constraints: Vector<Constraint>
    val size: Int

    /**
     * Returns the coefficients of the constraints of the
     * optimization problem as a matrix
     */
    constructor(vararg constraints: Constraint) {
        this.constraints = vectorFromArray(constraints as Array<Constraint>)
        this.size = constraints.size
    }

    fun toMatrix(): FractionMatrix? {
        if (constraints.size <= 0 || !checkConstraintsSize() || !checkConstraintsRelation()) return null
        val vectorOfVectors = FractionMatrix()
        return vectorOfVectors
    }

    /**
     * Checks whether the constraints sizes are equal,
     * because in case when sizes differ we can't solve the
     * optimization problem
     */
    private fun checkConstraintsSize(): Boolean {
        val size = constraints[0].coefficients.size
        return constraints.all { it.coefficients.size == size }
    }

    /**
     *
     */
    private fun checkConstraintsRelation(): Boolean {
        return constraints.any { it.relation != Constraint.Relation.EQ }
    }

    fun normalize() {
        constraints.forEach { it.normalize() }
    }

}