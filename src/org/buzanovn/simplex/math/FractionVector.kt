package org.buzanovn.simplex.math

import java.io.Serializable

/**
 * Representation of the Vector class that can store
 * only concrete double values based on the [Vector] class
 */
class FractionVector : Vector<KFraction>, Serializable {

    /**
     * Inherits the constructor of the superclass
     * and instantiates an empty vector of zero size
     */
    constructor() : super()

    /**
     * Inherits the constructor of the superclass
     * and instantiates a vector filled with 0 of [size] length
     */
    constructor(size: Int) : super(size, KFraction.ZERO)

    /**
     * Inherits the constructor of the superclass
     * and instantiates a vector filled with [default] value of [size] length
     */
    constructor(size: Int, default: KFraction) : super(size, default)

    /**
     * Inherits the constructor of superclass
     * and instantiates a vector from a vararg [fractions] array
     */
    constructor(vararg fractions: KFraction) {
        if (fractions.isEmpty()) throw IllegalArgumentException("Too less arguments")
        this.size = fractions.size
    }

    /**
     * Inverts the signs of vector elements
     */
    fun invert() {
        this.forEach { it * KFraction.MINUS_ONE }
    }

    /**
     * Checks whether the vectors elements are all positive
     */
    fun isPositive(): Boolean {
        return this.all { it >= KFraction.ZERO }
    }
}