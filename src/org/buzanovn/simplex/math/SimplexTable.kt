package org.buzanovn.simplex.math

import org.apache.commons.math3.fraction.Fraction

class SimplexTable(val objectiveFunction: ObjectiveFunction,
                   coefficientMatrix: Constraints,
                   val basicSolution: FractionVector,
                   var numDecisionVariables: Int,
                   var numSlackVariables: Int) {
    val coefficientMatrix: FractionMatrix
    lateinit var columnLabels: MutableList<String>
    lateinit var rowLabels: MutableList<String>

    /**
     * Initialize labels of the table
     */
    private fun initLabels(termsAmount: Int, constraintsAmount: Int) {
        columnLabels = mutableListOf()
        for (i in 0..termsAmount - 1) {
            columnLabels.add("x_" + (i + 1))
        }
        rowLabels = mutableListOf()
        for (j in 0..constraintsAmount - 1) {
            rowLabels.add("x_" + (termsAmount + j + 1))
        }
    }

    /**
     * Checks whether the simplex table is now optimal
     */
    fun isOptimized(): Boolean {
        return this.objectiveFunction!!.coefficients.isPositive()
    }

    fun getObjectiveElementPosition(): Pair<Int, Int> {
        return Pair(getObjectiveRowNumber(), getObjectiveColumnNumber())
    }

    private fun getObjectiveColumnNumber(): Int {
        return objectiveFunction.coefficients.array.indexOfFirst { it < Fraction.ZERO }
    }

    private fun getObjectiveRowNumber(): Int {
        val column: FractionVector = coefficientMatrix.getColumn(getObjectiveColumnNumber()) as FractionVector
        var minimalRatioPosition = 0
        var minimalRatio = column[0] / basicSolution[0]
        for (i in 0..coefficientMatrix.columnSize - 1) {
            if (column[i] / basicSolution[i] < minimalRatio && column[i] >= Fraction.ZERO) {
                minimalRatioPosition = i
                minimalRatio = column[i] / basicSolution[i]
            }
        }
        return minimalRatioPosition
    }


    override fun toString(): String {
        var result: String = ""
        val m = coefficientMatrix
        for (i in 0..m.columnSize - 1) {
            result += m.getRow(i).toString()
            result += "|  " + basicSolution[i].toString()
        }
        return result
    }

    init {
        this.coefficientMatrix = coefficientMatrix.toMatrix()!!
        initLabels(objectiveFunction.coefficients.size,
                coefficientMatrix.size)
    }
}