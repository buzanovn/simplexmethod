package org.buzanovn.simplex.math

import java.util.*

class SimplexSolver {
    //TODO the idea of solver is based on stack which will allow you to go back in time
    lateinit var solverStack: Stack<SimplexTable>
    /**
     * Maximum amount of iterations in simplex solver iterations cycle
     */
    private val MAXIMUM_ITERATIONS: Int = Int.MAX_VALUE

    /**
     * Optimizes a given simplex table
     */
    fun optimize(table: SimplexTable) {

    }

    private fun doIteration(table: SimplexTable) {
        if (table.isOptimized()) return
        val objectiveElementPosition = table.getObjectiveElementPosition()
        print(objectiveElementPosition.toString())
    }

}