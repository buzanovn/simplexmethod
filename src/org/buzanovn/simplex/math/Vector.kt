package org.buzanovn.simplex.math

open class Vector<T> {
    /**
     * Size of the vector
     */
    var size: Int

    /**
     * The vector as array itself
     */
    lateinit var array: Array<T>

    /**
     * Instantiates an empty vector
     */
    constructor() {
        this.size = 0
    }

    constructor(size: Int) {
        this.size = size
        this.array = arrayOfNulls(size)
    }

    constructor(size: Int, default: T) {
        this.size = size
        this.array = arrayOfDefaults(size, default)
    }

    @Suppress("UNCHECKED_CAST")
    constructor(vararg elements: T) {
        this.size = elements.size
        this.array = elements as Array<T>
    }

    override fun toString(): String {
        var result: String = "{"
        if (size > 0) {
            for (i in 0..size - 1) {
                result += array[i].toString()
                if (i != size - 1) {
                    result += ", "
                }
            }
        }
        return result.plus("}")
    }

    fun any(predicate: (T) -> Boolean): Boolean {
        return this.array.any(predicate)
    }

    fun all(predicate: (T) -> Boolean): Boolean {
        return this.array.all(predicate)
    }

    operator fun get(i: Int): T {
        return this.array[i]
    }

    operator fun set(i: Int, value: T) {
        this.array[i] = value
    }

    fun forEachIndexed(action: (i: Int, element: T) -> Unit) {
        this.array.forEachIndexed { i, element -> action.invoke(i, element) }
    }

    fun forEach(action: (element: T) -> Unit) {
        this.array.forEach(action)
    }

    fun push(element: T) {
        this.size++
        this.array = copyAndExpandArray(this.array, this.size)
        this.array[this.size - 1] = element

    }

}