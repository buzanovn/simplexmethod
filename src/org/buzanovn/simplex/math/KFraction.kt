package org.buzanovn.simplex.math

import org.apache.commons.math3.fraction.Fraction

/**
 * Boxes the existing Fraction class
 * because of Kotlin's not-avaliable feature
 * of adding static extension methods
 */
class KFraction(numerator: Int, denominator: Int) : Fraction(numerator, denominator) {

    companion object {
        val ZERO = Fraction.ZERO as KFraction
        val ONE = Fraction.ONE as KFraction
        val MINUS_ONE = Fraction.MINUS_ONE as KFraction
    }

    constructor(fractionString: String) : this(0,1) {
        if (fractionString.isNullOrEmpty()){
            ZERO
        }
        if (!fractionString.contains('/')){
            KFraction(Integer.parseInt(fractionString), 1)
        } else {
            val fractionSplitted = fractionString.replace(" ", "", true).split("/", ignoreCase = false, limit = 0)
            if (fractionSplitted.size > 2) {
                throw IllegalArgumentException("Wrong type of fraction used")
            } else {
                KFraction(Integer.parseInt(fractionSplitted[0]), Integer.parseInt(fractionSplitted[1]))
            }
        }
    }

    operator fun plus(other: KFraction) : KFraction {
        return this.add(other) as KFraction
    }

    operator fun minus(other: KFraction) : KFraction {
        return this.subtract(other) as KFraction
    }

    operator fun times(other: KFraction) : KFraction {
        return this.multiply(other) as KFraction
    }

    operator fun div(other: KFraction) : Fraction {
        return this * other.reciprocal() as KFraction
    }

    override fun toByte(): Byte {
        throw UnsupportedOperationException("The operation is not implemented, because it is not present in super class" +
                "and not needed for the usage, but Kotlin won't compile it without being it implemented")
    }

    override fun toShort(): Short {
        throw UnsupportedOperationException("The operation is not implemented, because it is not present in super class" +
                "and not needed for the usage, but Kotlin won't compile it without being it implemented")
    }
}