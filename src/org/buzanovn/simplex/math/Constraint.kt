package org.buzanovn.simplex.math

/**
 * Representation of mathematical constraint
 * in reference to an optimization problem
 */
class Constraint(val coefficients: FractionVector,
                 val relation: Relation,
                 val relationCoefficient: KFraction) {

    override fun toString(): String {
        var result: String = ""
        var i = 1
        for (j in 0..coefficients.size - 1) {
            result += (coefficients[j].toString() + "*x_" + i + " + ")
            i++
        }
        result += (" " + relation.sign + " " + relationCoefficient)
        return result
    }

    fun normalize(): Boolean {
        if (relationCoefficient < KFraction.ZERO) {
            coefficients.invert()
            return true
        }
        return false
    }

    /**
     * Simple relation class, represents
     * all three types of possible constraint relations
     */
    enum class Relation(val sign: String) {
        LEQ("<="),
        GEQ(">="),
        EQ("=")
    }
}