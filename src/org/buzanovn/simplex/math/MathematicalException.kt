package org.buzanovn.simplex.math

class MathException(val exceptionType: MathExceptionType) : ArithmeticException(exceptionType.toString())

enum class MathExceptionType(private val message: String) {
    DIFFERENT_SIZES("An error occurred because the sizes differ"),
    TOO_FEW("Too few arguments while initializing"),
    TOO_MUCH("Too much arguments while initializing");

    override fun toString(): String {
        return this.message
    }
}
