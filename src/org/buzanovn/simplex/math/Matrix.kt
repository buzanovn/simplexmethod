package org.buzanovn.simplex.math

/**
 * Representation of mathematical object matrix that can store
 * any type of elements and provides basic actions and operations
 */
open class Matrix<R> {
    /**
     * The size of each row in matrix
     */
    var rowSize: Int

    /**
     * The size of each column in matrix, also the
     * number of one-dimensional vectors in it
     */
    var columnSize: Int

    /**
     * The matrix values itself stored in vector
     * of vectors of the element type given
     */
    lateinit var matrix: Vector<Vector<R>>

    /**
     * Instantiates an empty matrix
     */
    constructor() {
        rowSize = 0
        columnSize = 0
    }

    /**
     * Instantiates a matrix filled with zeros
     * of [columnSize] and [rowSize] dimensions
     */
    constructor(columnSize: Int, rowSize: Int) {
        this.rowSize = rowSize
        this.columnSize = columnSize
        matrix = matrixOfNulls(columnSize, rowSize)
    }

    /**
     * Instantiates a matrix filled with [default] value
     * of [columnSize] and [rowSize] dimensions
     */
    constructor(columnSize: Int, rowSize: Int, default: R) {
        this.rowSize = rowSize
        this.columnSize = columnSize
        this.matrix = matrixOfDefaults(columnSize, rowSize, default)
    }

    constructor(vararg vectors: Vector<R>) {
        if (vectors.isEmpty() || vectors.any { it.size != vectors[0].size })
            throw IllegalArgumentException("The sizes of vectors are incorrect")
        this.columnSize = vectors.size
        this.rowSize = vectors[0].size
        this.matrix = Vector(columnSize)
        for (i in 0..columnSize - 1) {
            this.matrix[i] = vectors[i]
        }
    }

    override fun toString(): String {
        var result = "{\n"
        for (i in 0..columnSize - 1) {
            result += matrix[i].toString()
            if (i != columnSize - 1) {
                result += ",\n"
            }
        }
        return result.plus("\n}")
    }

    open operator fun get(i: Int, j: Int): R {
        return this.matrix[i][j]
    }

    open operator fun set(i: Int, j: Int, value: R) {
        (this.matrix[i])[j] = value
    }

    open fun reduceElement(row: Int, column: Int): Matrix<R>? {
        if (row > rowSize || column > columnSize) {
            return null
        }
        val result = Matrix<R>(rowSize - 1, columnSize - 1)
        var columnOld = 0
        var rowOld = 0
        var columnNew = 0
        var rowNew = 0
        while (columnOld < columnSize) {
            if (column != columnOld) {
                while (rowOld < rowSize) {
                    if (row != rowOld) {
                        result[columnNew, rowNew] = this[columnOld, rowOld]
                        rowNew++
                    }
                    rowOld++
                }
                columnNew++
                rowNew = 0
            }
            columnOld++
            rowOld = 0
        }
        return result
    }

    fun getRow(i: Int): Vector<R> {
        return this.matrix[i]
    }

    fun setRow(i: Int, value: Vector<R>) {
        if (value.size != rowSize) throw IllegalArgumentException("Wrong size of the vector that it tried to be set")
        this.matrix[i] = value
    }

    fun getColumn(j: Int): Vector<R> {
        val vector = Vector<R>(columnSize)
        for (i in 0..columnSize - 1) {
            vector[i] = this[i, j]
        }
        return vector
    }

    fun map(action: (matrix: Matrix<R>) -> Unit): Matrix<R> {
        action.invoke(this)
        return this
    }

}