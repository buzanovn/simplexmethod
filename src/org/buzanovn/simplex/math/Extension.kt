package org.buzanovn.simplex.math

/**
 * Replaces the standard Kotlin array of nulls
 * returning function that can't be cast to a generic parameter
 */
@Suppress("UNCHECKED_CAST")
fun <T> arrayOfNulls(size: Int): Array<T> {
    return Array<Any?>(size, { null }) as Array<T>
}

@Suppress("UNCHECKED_CAST")
fun <T> arrayOfDefaults(size: Int, default: T): Array<T> {
    return Array<Any?>(size, { default }) as Array<T>
}

fun <R> matrixOfNulls(columnSize: Int, rowSize: Int): Vector<Vector<R>> {
    val vectorOfVectors = Vector<Vector<R>>(columnSize)
    for (i in 0..columnSize - 1) {
        vectorOfVectors[i] = Vector<R>(rowSize)
    }
    return vectorOfVectors
}

fun <T> matrixOfDefaults(columnSize: Int, rowSize: Int, default: T): Vector<Vector<T>> {
    val vectorOfVectors = Vector<Vector<T>>(columnSize)
    for (i in 0..columnSize - 1) {
        vectorOfVectors[i] = Vector(rowSize, default)
    }
    return vectorOfVectors
}

fun <T> vectorFromArray(array: Array<T>): Vector<T> {
    val vector = Vector<T>(array.size)
    vector.array = array
    return vector
}

fun <T> vectorFromList(list: List<T>): Vector<T> {
    val vector = Vector<T>(list.size)
    return Vector()
    //TODO
}

fun <T> copyAndExpandArray(array: Array<T>, newSize: Int): Array<T> {
    val oldArray = array
    val newArray = arrayOfNulls<T>(newSize)
    oldArray.mapIndexed { i, t -> newArray[i] = t }
    return newArray
}
