package org.buzanovn.simplex.math

class FractionMatrix : Matrix<KFraction> {
    constructor() : super()

    constructor(columnSize: Int, rowSize: Int) : super(columnSize, rowSize, KFraction.ZERO)

    constructor(columnSize: Int, rowSize: Int, default: KFraction) : super(columnSize, rowSize, default)

    constructor(vararg vectors: FractionVector) {
        if (vectors.isEmpty() || vectors.any { it.size != vectors[0].size })
            throw IllegalArgumentException("The sizes of vectors are incorrect")
        this.columnSize = vectors.size
        this.rowSize = vectors[0].size
        this.matrix = Vector(columnSize)
        for (i in 0..columnSize - 1) {
            this.matrix[i] = vectors[i]
        }
    }

    operator fun plus(other: FractionMatrix): FractionMatrix {
        if (this.rowSize != other.rowSize || this.columnSize != other.columnSize) {
            throw MathException(MathExceptionType.DIFFERENT_SIZES)
        }
        for (i in 0..columnSize - 1) {
            for (j in 0..rowSize - 1) {
                this[i, j] += other[i, j]
            }
        }
        return this
    }

    operator fun minus(other: FractionMatrix): FractionMatrix {
        if (this.rowSize != other.rowSize || this.columnSize != other.columnSize) {
            throw MathException(MathExceptionType.DIFFERENT_SIZES)
        }
        for (i in 0..columnSize - 1) {
            for (j in 0..rowSize - 1) {
                this[i, j] -= other[i, j]
            }
        }
        return this
    }

    operator fun times(other: FractionMatrix): FractionMatrix {
        //TODO check sizes of matrices while multiplying
        val tmpMatrix = FractionMatrix(this.rowSize, this.columnSize)
        for (i in 0..columnSize - 1) {
            for (j in 0..rowSize - 1) {
                for (k in 0..rowSize - 1) {
                    tmpMatrix[i, j] += this[i, k] * other[k, j]
                }
            }
        }
        return tmpMatrix
    }

    operator fun times(other: KFraction): FractionMatrix {
        for (column in 0..columnSize - 1) {
            for (row in 0..rowSize - 1) {
                this[column, row] *= other
            }
        }
        return this
    }

    companion object {
        /**
         * Returns an identity matrix of given size
         */
        fun identityMatrix(size: Int): FractionMatrix {
            return FractionMatrix(size, size).map {
                for (i in 0..size - 1) {
                    it[i, i] = KFraction.ONE
                }
            } as FractionMatrix
        }
    }
}