package org.buzanovn.simplex

interface Callback {
    fun next()
}