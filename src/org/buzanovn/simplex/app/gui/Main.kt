package org.buzanovn.simplex.app.gui

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import org.buzanovn.simplex.app.gui.controller.MainController

class Main : Application() {
    val layout = "main_window_layout.fxml"

    override fun start(primaryStage: Stage?) {
        val loader = FXMLLoader()
        loader.location = javaClass.getResource(layout)
        val controller = MainController()
        controller.currentStage = primaryStage ?: return
        loader.setController(controller)
        val root = loader.load<Parent>()
        primaryStage.title = "Simplex Method Calculator"
        primaryStage.scene = Scene(root)
        primaryStage.show()
    }

    /**
     * A Kotlin-based implementation of main method call
     * referenced by JavaFX
     */
    companion object {
        @JvmStatic
        fun main(args: Array<String>){
            try {
                Application.launch(Main::class.java)
            } catch(exception: Exception) {
                print("An exception \"${exception.message}\" occurred \n" +
                        "and was caused by \"${exception.cause}\". This is the stack trace:\n ")
                exception.printStackTrace()
            }
        }
    }
}