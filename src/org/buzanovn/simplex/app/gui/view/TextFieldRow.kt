package org.buzanovn.simplex.app.gui.view

import javafx.collections.FXCollections
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import org.buzanovn.simplex.math.*
import java.util.*

class KFractionTextField(size: Int) : TextField() {
    init {
        this.prefWidth = (50 + (10 / size)).toDouble()
        this.alignment = Pos.CENTER
    }

    fun getKFraction() : KFraction {
        var fraction : KFraction? = null
        try {
            fraction = KFraction(this.text)
        } catch(exception: Exception) {
            exception.printStackTrace()
        } finally {
            if (fraction != null) {
                return fraction
            } else return KFraction.ZERO
        }
    }
}

abstract class TextFieldRow(val size: Int) : HBox() {
    val fieldList: MutableList<TextField>

    init {
        fieldList = ArrayList<TextField>(size)
        this.spacing = 10.0
    }

    protected fun addNode(node: Node?) {
        this.children.add(node)
    }
}

class ObjectiveFunctionRow(size: Int) : TextFieldRow(size) {
    val absoluteTermField = KFractionTextField(size)
    val problemTypeChoiceBox = ChoiceBox(
            FXCollections.observableArrayList(ObjectiveFunction.ProblemType.MIN, ObjectiveFunction.ProblemType.MAX)
    )

    init {
        for (i in 0..size - 1) {
            val textField = KFractionTextField(size)
            fieldList.add(textField)
            addNode(textField)
        }
        addNode(absoluteTermField)
        addNode(Label("->"))
        addNode(problemTypeChoiceBox)
    }
}


/*fun obtainFunction() : ObjectiveFunction {
    val f = ObjectiveFunction()
    return f
}*/


class ConstraintRow(rowSize: Int) : TextFieldRow(rowSize) {
    val relationCoefficientTextField = KFractionTextField(rowSize)

    init {
        for (i in 0..size - 1) {
            val textField = KFractionTextField(rowSize)
            fieldList.add(textField)
            addNode(textField)
        }
        addNode(Label("="))
        addNode(relationCoefficientTextField)
    }

    fun getConstraint(): Constraint {
        val vec = FractionVector()
        fieldList.forEach {vec.push((it as KFractionTextField).getKFraction())}
        return Constraint(vec, Constraint.Relation.EQ, KFraction(relationCoefficientTextField.text))
    }
}

class ConstraintsRows(rowSize: Int, columnSize: Int) : VBox() {

    private fun addRow(constraintRow: ConstraintRow?) {
        this.children.add(constraintRow)
    }

    fun obtainConstraints(): Constraints {
        val constraintList: MutableList<Constraint> = mutableListOf()
        this.children.forEach {
            if (it is ConstraintRow) {
                constraintList.add(it.getConstraint())
            }
        }
        return Constraints()
    }

    init {
        for (i in 0..columnSize - 1) {
            this.addRow(ConstraintRow(rowSize))
        }
    }
}