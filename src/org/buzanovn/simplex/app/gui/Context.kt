package org.buzanovn.simplex.app.gui

import java.io.Serializable
import java.util.*

class Context private constructor() {
    private object InstanceHolder {
        val instance = Context()
    }

    private val map : HashMap<String,Serializable> = HashMap()

    fun <T : Serializable> put(key: String, value: T) {
        map.put(key, value)
    }

    fun <T : Serializable> get(key: String) : T {
        val obj = map.get(key)
        map.remove(key)
        return obj as T
    }

    companion object {
        val instance: Context by lazy { InstanceHolder.instance }
    }


}