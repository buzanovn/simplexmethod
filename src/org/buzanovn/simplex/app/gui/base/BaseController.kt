package org.buzanovn.simplex.app.gui.base

import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle

abstract class BaseController {
    lateinit var currentStage: Stage
    protected val loader = FXMLLoader()

    fun <T : BaseController> load(fxmlPath: String, type: WindowType) {
        loader.location = javaClass.getResource(fxmlPath)
        loader.load<Parent>()
        val root = loader.getRoot<Parent>()
        val controller = loader.getController<T>()
        val scene = Scene(root)
        val stage: Stage
        when (type) {
            WindowType.SCENE -> {
                stage = currentStage
            }
            WindowType.DIALOG -> {
                stage = Stage(StageStyle.DECORATED)
                stage.initModality(Modality.WINDOW_MODAL)
                stage.initOwner(currentStage)
            }
        }
        stage.scene = scene
        controller.currentStage = stage
        loader.setController(controller)
        stage.show()
    }


    enum class WindowType(private val type: Int) {
        SCENE(0), DIALOG(1)
    }

}
