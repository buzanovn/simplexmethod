package org.buzanovn.simplex.app.gui.controller

import javafx.fxml.Initializable
import java.net.URL
import java.util.*


class MainController() : BaseController(), Initializable {

    override fun initialize(location: URL?, resources: ResourceBundle?) {
//        loadWithCallback<StartDialogController>("start_dialog_controller.fxml", WindowType.DIALOG, object : Callback {
//            override fun next() {
//                println(context.get<Int>("objfunction"))
//                println(context.get<Constraints>("constr"))
//            }
//
//        })
        load<StartDialogController>("start_dialog_controller.fxml", WindowType.DIALOG)
    }
}
