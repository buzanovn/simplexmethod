package org.buzanovn.simplex.app.gui.controller

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.layout.VBox
import org.buzanovn.simplex.app.gui.view.ConstraintsRows
import org.buzanovn.simplex.app.gui.view.ObjectiveFunctionRow
import java.net.URL
import java.util.*

class StartDialogController() : BaseController(), Initializable {

    @FXML
    lateinit var termSizeChoiceBox: ChoiceBox<Int>

    @FXML
    lateinit var constraintSizeChoiceBox: ChoiceBox<Int>

    @FXML
    lateinit var objectiveFunctionContainer: VBox

    @FXML
    lateinit var constraintsContainer: VBox

    @FXML
    lateinit var submitDataButton: Button

    val observableChoiceItems: ObservableList<Int> by lazy { FXCollections.observableArrayList(
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
    )}

    val functionLabel = Label("Целевая функция")

    val constraintsLabel = Label("Ограничения")

    lateinit var objectiveFunctionRow: ObjectiveFunctionRow

    lateinit var constraintsRow: ConstraintsRows

    /**
     * The action that will take place when the [termSizeChoiceBox]
     * of [constraintSizeChoiceBox] is used to change the
     * number of terms or constraints
     */
    val choiceBoxesAction: (ActionEvent) -> Unit = {
        var termSize = termSizeChoiceBox.selectionModel.selectedItem
        var constraintSize = constraintSizeChoiceBox.selectionModel.selectedItem
        if (termSize != null && constraintSize != null) {
            initObjectiveFunctionRow(termSize)
            initConstraintsRow(termSize,constraintSize)
            submitDataButton.isDisable = false
            currentStage.sizeToScene()
        }
    }

    /**
     * The action will take place when the [submitDataButton]
     * will be clicked in case the [choiceBoxesAction] has happened
     */
    val submitDataButtonAction : (ActionEvent) -> Unit = {
        context.put("objfunction", objectiveFunctionRow.size)
        context.put("constr", constraintsRow.obtainConstraints())
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        termSizeChoiceBox.items = observableChoiceItems
        constraintSizeChoiceBox.items = observableChoiceItems

        termSizeChoiceBox.setOnAction(choiceBoxesAction)
        constraintSizeChoiceBox.setOnAction(choiceBoxesAction)

        objectiveFunctionContainer.isVisible = false
        constraintsContainer.isVisible = false

        submitDataButton.setOnAction(submitDataButtonAction)
    }


    private fun initConstraintsRow(termSize: Int, constraintSize : Int)
    {
        constraintsContainer.isVisible = true
        constraintsContainer.children.clear()
        constraintsRow = ConstraintsRows(termSize, constraintSize)
        constraintsContainer.children.addAll(constraintsLabel, constraintsRow)
    }

    private fun initObjectiveFunctionRow(termSize: Int) {
        objectiveFunctionContainer.isVisible = true
        objectiveFunctionContainer.children.clear()
        objectiveFunctionRow = ObjectiveFunctionRow(termSize)
        objectiveFunctionContainer.children.addAll(functionLabel, objectiveFunctionRow)
    }


}
